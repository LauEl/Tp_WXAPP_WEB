<?php

namespace app\api\controller;

use think\Controller;
use think\Db;
use think\Config;

class Index extends Controller
{


    public function index()
    {
        //定义接口
        return '定义接口';
    }


    //分页获取
    public function index_article_list()
    {
        $page = input('page');
        return json(['code' => '0', 'page' => $page]);
    }


    public function article()
    {
        $res = Db::name('data')->order('id desc')->select();
        for ($i = 0; $i < count($res); $i++) {
            $where = '';
            $where['id'] = $res[$i]['img'];
            $temp = Db::name('attachment')->where($where)->find();
            $res[$i]['filepath'] = 'https://www.yxxxm.com' . $temp['filepath'];
        }
        return json(['code' => 0, 'msg' => '加载成功！', 'data' => $res]);

    }


    public function article_info()
    {
        $id = input('id');
        $where = "";
        $where['id'] = $id;
        $res = Db::name('data')->where($where)->order('id desc')->order('create_time desc')->find();
        $content = $res['content'];
        $content = str_replace('/data', 'https://www.yxxxm.com/data', $content);
        $res['content'] = $content;
        return json(['code' => 0, 'msg' => '加载成功！', 'data' => $res]);
    }

    //获取文章分类
    public function article_cate()
    {
        $res = Db::name('data_cate')->select();

        return json(['code' => 0, 'msg' => '加载成功！', 'data' => $res]);

    }

//获取文章分类下的文章列表
    public function getarticlelist()
    {
        $article_cate_id = input('article_cate_id');
        if (!empty($article_cate_id)) {
            $where = array();
            $where['data_cate_id'] = $article_cate_id;
            $res = Db::name('data')->where($where)->order('id desc')->order('create_time desc')->select();
            for ($i = 0; $i < count($res); $i++) {
                $where = '';
                $where['id'] = $res[$i]['img'];
                $temp = Db::name('attachment')->where($where)->find();
                $res[$i]['filepath'] = 'https://www.yxxxm.com' . $temp['filepath'];
            }

            return json(['code' => 0, 'msg' => '加载成功！', 'data' => $res]);
        } else {

            $res = Db::name('data')->order('id desc')->order('create_time desc')->select();
            for ($i = 0; $i < count($res); $i++) {
                $where = '';
                $where['id'] = $res[$i]['img'];
                $temp = Db::name('attachment')->where($where)->find();
                $res[$i]['filepath'] = 'https://www.yxxxm.com' . $temp['filepath'];
            }
            return json(['code' => 0, 'msg' => '加载成功！', 'data' => $res]);

        }


    }

    /**
     * 在富文本编辑器中 获得图片  返回全路径数组
     */
    function getContentImg($content)
    {
        $preg = '/<img.*?src=[\"|\']?(.*?)[\"|\']?\s.*?>/i';
        preg_match_all($preg, $content, $match);
        $count = count($match[1]);
        $filepaths = '';
        if ($count > 0) {
            for ($i = 0; $i < $count; $i++) {
                if ($i != $count - 1) {
                    $filepaths .= $match[1][$i] . ',';
                } else {
                    $filepaths .= $match[1][$i];
                }
            }
        }
        return $filepaths;
    }

    //json返回
    public function json($code, $msg = "", $count, $data = array())
    {
        $result = array(
            'code' => $code,
            'msg' => $msg,
            'count' => $count,
            'data' => $data
        );
        //输出json
        echo json_encode($result);
        exit;
    }


}
